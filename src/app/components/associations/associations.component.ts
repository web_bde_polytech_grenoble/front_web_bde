import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AssociationDetailComponent } from './association-detail/association-detail.component';
import { Association } from '@class/association'
import { ContentDataService } from '@services/content-data.service';

@Component({
  selector: 'app-associations',
  templateUrl: './associations.component.html',
  styleUrls: ['./associations.component.css', '../../shared/css/card.css']
})
export class AssociationsComponent implements OnInit {
  _associations : Association[];
  _cellIndep : Association[];
  imgFolderPath : string;

  error : boolean = false;
  load : boolean = false;

  constructor(
    private dialog: MatDialog, 
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.imgFolderPath = 'assets/icons/associations/';
  }

  async loadData(){
    let data = await this.contentDataService.getAssociationsData();
    this._associations = data["associationsList"];
    this._cellIndep = data["cellulesIndependantes"];
    this.error = data['error']; 
    this.load = true;
  }

  openCard(ass : Association): void{       
    const dialogRef = this.dialog.open(AssociationDetailComponent, {
      autoFocus : true,
      data : {
        association : ass
      }
    });
  }
}
