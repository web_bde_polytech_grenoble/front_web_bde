import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { Association } from '@class/association';

@Component({
  selector: 'app-association-detail',
  templateUrl: './association-detail.component.html',
  styleUrls: ['./association-detail.component.css', '../../../shared/css/dialog.css']
})
export class AssociationDetailComponent implements OnInit {

  association : Association;
  imgFolderPath : string;

  constructor(public dialogRef: MatDialogRef<AssociationDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.association = data.association
   }

  ngOnInit(): void {
    this.imgFolderPath = 'assets/icons/associations/';
  }

}
