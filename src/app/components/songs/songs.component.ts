import { Component, OnInit } from '@angular/core';
import { Song } from '@class/song';
import { SongDetailComponent } from './song-detail/song-detail.component';
import { MatDialog } from '@angular/material/dialog';
import { ContentDataService } from '@shared/services/content-data.service';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css', '../../shared/css/card.css']
})
export class SongsComponent implements OnInit {
  _songs : Song[];

  error : boolean = false;
  load : boolean = false;

  constructor(
    private dialog: MatDialog,
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(){
    let data = await this.contentDataService.getSongsData();
    this._songs = data['songsList'];
    this.error = data['error']; 
    this.load = true;

    // this.load = false;
    // this.error = true;
    // this._songs = [];
  }

  openCard(sg : Song): void{       
    const dialogRef = this.dialog.open(SongDetailComponent, {
      autoFocus : true,
      data : {
        song : sg
      }
    });
  }
}
