import { Component, OnInit, Inject } from '@angular/core';
import { Song } from '@class/song';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { Link } from '@class/link'
import { DeepLinkService } from '@services/deep-link.service';

@Component({
  selector: 'app-song-detail',
  templateUrl: './song-detail.component.html',
  styleUrls: ['./song-detail.component.css']
})
export class SongDetailComponent implements OnInit {

  song : Song;

  constructor(public dialogRef: MatDialogRef<SongDetailComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private deepLink : DeepLinkService) { 
    this.song = data.song;
  }

  ngOnInit(): void {
  }

  open(link : Link){
    this.deepLink.deepLink(link);
  }
}
