import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-the-city',
  templateUrl: './the-city.component.html',
  styleUrls: ['./the-city.component.css']
})
export class TheCityComponent implements OnInit {

  error : boolean = false;
  load : boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(){
    this.error = false; 
    this.load = true;
  }
}
