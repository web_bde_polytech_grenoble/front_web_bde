import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheCityComponent } from './the-city.component';

describe('TheCityComponent', () => {
  let component: TheCityComponent;
  let fixture: ComponentFixture<TheCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
