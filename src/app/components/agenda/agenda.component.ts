import { Component, OnInit } from '@angular/core';

export const STORAGE_EDT_ID_KEY = "edtID";

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  
  constructor() { }

  ngOnInit(): void {
  }

}
