import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FullCalendarModule } from '@fullcalendar/angular';
import fullcalendarTimegridPlugin from '@fullcalendar/timegrid';
FullCalendarModule.registerPlugins([
  fullcalendarTimegridPlugin
]);

import { WebStorageModule } from 'ngx-store';

import { AgendaComponent } from './agenda.component';
import { AgendaRoutingModule } from './agenda-routing.module';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// COMPONENTS
import { InfoModalComponent } from './components/info-modal/info-modal.component';

// PAGES
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { EdtPageComponent } from './pages/edt-page/edt-page.component';
import { PickPageComponent } from './pages/pick-page/pick-page.component';

import { environment } from '../../../environments/environment';
import { InfoModalModule } from './components/info-modal/info-modal.component.module';
import { FormModule } from './components/form/form.component.module';


@NgModule({
    declarations: [
      AgendaComponent,
      PageNotFoundComponent,
      EdtPageComponent,
      PickPageComponent,
    ],
    imports: [
      BrowserModule,
      BrowserAnimationsModule,
      AgendaRoutingModule,
      FullCalendarModule,
      WebStorageModule,
      MatDialogModule,
      MatFormFieldModule,
      MatSelectModule,
      MatOptionModule,
      MatButtonModule,
      MatIconModule,
      MatCardModule,
      MatToolbarModule,
      MatDividerModule,
      MatSidenavModule,
      MatProgressSpinnerModule,
      InfoModalModule,
      FormModule,
      MatExpansionModule
    ],
    entryComponents: [
      InfoModalComponent,
    ],
    providers: [],
    bootstrap: [
      AgendaComponent
    ]
  })

  export class AgendaModule {}