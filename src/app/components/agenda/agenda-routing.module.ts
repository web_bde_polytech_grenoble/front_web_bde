import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { EdtPageComponent } from './pages/edt-page/edt-page.component';
import { PickPageComponent } from './pages/pick-page/pick-page.component';

const agendaRoutes: Routes = [
  { path: '', redirectTo: 'edt', pathMatch: 'full' },
  { path: 'pick', component: PickPageComponent },
  { path: 'edt', component: EdtPageComponent },
  { path: 'edt/:ids', component: EdtPageComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [ 
    // RouterModule.forRoot(agendaRoutes, {useHash: true}),
    RouterModule.forChild(agendaRoutes) 
  ],
  exports: [ 
    RouterModule 
  ]
})
export class AgendaRoutingModule {}
