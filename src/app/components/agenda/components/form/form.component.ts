import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ContentDataService } from '@services/content-data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit{
  listEnglishGrp;
  resources;

  selectedClass: string;
  selectedYear: string;
  selectedGroups: string[];
  selectedEnglishGrp: string;

  load : boolean = false;
  error : boolean = false;

  @Output() selected = new EventEmitter<string[]>();

  constructor(
    private contentDataService : ContentDataService, 
  ) {}

  ngOnInit() {
    this.loadData();
  }

  async loadData(){
    let res1 = await this.contentDataService.getAgendaEnglishGroup();
    let res2 = await this.contentDataService.getAgendaBranchs();

    this.listEnglishGrp = res1["englishGroups"];
    this.resources = res2["branchsList"];
    this.error = res1["error"] || res2["error"];
    this.load = true;
  }

  emit() {
    if (!this.selectedGroups) { 
      return; 
    }
    const ids = this.selectedGroups;
    ids.push(this.selectedEnglishGrp);
    this.selected.emit(ids);
  }
}
