import { Component, OnInit, ViewChild, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ContentDataService } from '@services/content-data.service';

import { EventInput } from '@fullcalendar/core';
import { FullCalendarComponent, CalendarOptions } from '@fullcalendar/angular';
import { MatDialog } from '@angular/material/dialog';

import { InfoModalComponent } from '../../components/info-modal/info-modal.component';
import { unionWith } from 'lodash';

import { WebStorageService } from '../../../../shared/services/web-storage.service';
import { STORAGE_EDT_ID_KEY } from '../../agenda.component';

@Component({
  selector: 'app-edt-page',
  templateUrl: './edt-page.component.html',
  styleUrls: ['./edt-page.component.scss']
})
export class EdtPageComponent implements OnInit {

  @ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent;
  events: EventInput[] = [];
  calInfos: {name: string, updated: Date}[] = [];

  error : boolean = false;
  load : boolean = false

  ids = [];

  calendarOption : CalendarOptions;

  constructor(
    private contentDataService : ContentDataService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private localStorge : WebStorageService
  ) {}

  ngOnInit() {
    this.calendarOption = {
      initialView : "timeGridWeek",
      events : this.events,
      editable : false,
      nowIndicator : true,
      height : 'auto',
      slotMinTime : '07:00:00',
      slotMaxTime : '20:00:00',
      locale : 'fr',
      allDaySlot : false, // can display a error on your IDE but that work
      weekends : false,
      headerToolbar : {
        left: 'prev today',
        center: 'title',
        right: 'next'
      },
      slotDuration : "00:15:00",
      eventClick : function(eventClick){
        alert(eventClick.event['extendedProps']['trueTitle'] + eventClick.event['extendedProps']['location']);

        // NOT WORKING
        // this.openInfo(eventClick);
      }
    };
    
    const routeIds = this.route.snapshot.paramMap.has('ids') ? this.route.snapshot.paramMap.get('ids').split(',') : [];
    if (routeIds.length === 0) {
      // edt-page.component : ID non trouvé par URL
      const localStorageIds = this.localStorge.getLocalStrorage(STORAGE_EDT_ID_KEY);
      if (localStorageIds && localStorageIds.length != 0){
        // edt-page.component : ID trouvé en localSorage -> "+localStorageIds
        this.loadCalendars(localStorageIds);
      }else{
        // edt-page.component : ID non trouvé en localSorage      
        this.navigateChild("pick")
      }
      return;
    }
    this.route.paramMap.subscribe(params => {
      const ids: string[] = params.has('ids') ? params.get('ids').split(',') : [];
      this.ids = ids;
      this.loadCalendars(ids);
    });
  }

  /**
   * Load all specified calendars into FullCalendar component events model
   * @param  {string[]} ids - Array of calendar ids to retrieve events from
   */
 async loadCalendars(ids: string[]){
    var data = await this.contentDataService.getAgenda(ids);
    this.error = data["error"];
    this.events = data["eventList"];

    // var data2 = await this.contentDataService.getAgendaInfo(ids);
    // this.calInfos = data2["agendaInfo"];

    this.load = true;

    // allow to update the table on the component FullEvent
    this.calendarOption.events = this.events;
 }

  public openInfo(ev) {
    this.dialog.open(InfoModalComponent, {
      data: {
        title: ev.event['extendedProps']['trueTitle'],
        description: ev.event['extendedProps']['description'],
        location: ev.event['extendedProps']['location']
      }
    });
  }

  navigateChild(child : string){
    let urlParent;
      this.route.parent.url.subscribe((urlPath) => {
        urlParent = urlPath[urlPath.length - 1].path;
      })
      this.router.navigate(['/'+urlParent+'/'+child]); 
  }
}
