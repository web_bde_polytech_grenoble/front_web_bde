import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { EdtPageComponent } from './edt-page.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { EdtService } from '../../services/edt.service';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';

describe('EdtPageComponent', () => {
  let component: EdtPageComponent;
  let fixture: ComponentFixture<EdtPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EdtPageComponent],
      imports: [
        FullCalendarModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatCardModule,
        MatExpansionModule,
        RouterTestingModule.withRoutes([
          { path: 'pick', component: EdtPageComponent }
        ])
      ],
      providers: [
        {
          provide: EdtService, useValue: {
            refresher: new Observable(),
            calendarIds: []
          }
        },
      ]
    }).compileComponents();
  }));

  beforeEach(async(
    inject([EdtService],
      () => {
        fixture = TestBed.createComponent(EdtPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      })));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
