import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebStorageService } from '../../../../shared/services/web-storage.service';
import { STORAGE_EDT_ID_KEY } from '../../agenda.component';

@Component({
  selector: 'app-pick-page',
  templateUrl: './pick-page.component.html',
  styleUrls: ['./pick-page.component.scss']
})
export class PickPageComponent implements OnInit {

  resources: Object = null;
  error = false;

  constructor(
    private router: Router, 
    private route : ActivatedRoute,
    private localStorage : WebStorageService) { }

  ngOnInit() {
    this.localStorage.removeLocalStorage(STORAGE_EDT_ID_KEY);
  }

  toCalendars(ids: string[]) {
    let urlParent;
    this.route.parent.url.subscribe((urlPath) => {
      urlParent = urlPath[urlPath.length - 1].path;
    })
    this.router.navigate(['/'+urlParent+'/edt', { ids: ids }]);

    this.localStorage.setLocalStorage(STORAGE_EDT_ID_KEY,ids);
  }
}
