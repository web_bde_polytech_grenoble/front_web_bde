import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  formData: FormGroup;

  constructor(private builder: FormBuilder, private http : HttpClient) { }

  ngOnInit(): void {
    this.formData = this.builder.group({
      Fullname: new FormControl('', [Validators.compose([Validators.required, Validators.minLength(3)])]),
      Email: new FormControl('', [Validators.compose([Validators.required, Validators.email])]),
      Message: new FormControl('', [Validators.compose([Validators.required, Validators.minLength(5)])])
      })
  }

  onSubmit() {
    // value of the form : this.formData.value
    if (this.formData.valid) {
      // Contact Form Submitted
      const res = this.sendMail(this.formData);
      this.formData.reset();
    }else{
      // Invalid Contact Form
      Object.keys(this.formData.controls).forEach(field => {
        const control = this.formData.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({ onlySelf: true });
         } 
      });
    }
  }

  sendMail(data : FormGroup){

  }
}
