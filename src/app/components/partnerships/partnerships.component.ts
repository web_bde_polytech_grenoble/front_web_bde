import { Component, OnInit } from '@angular/core';
import { partnerships, partnershipIntro } from '@data/partnership-data'
import { Partnership } from '@class/partnership';
import { ContentDataService } from '@shared/services/content-data.service';

@Component({
  selector: 'app-partnerships',
  templateUrl: './partnerships.component.html',
  styleUrls: ['./partnerships.component.css', '../../shared/css/card.css']
})
export class PartnershipsComponent implements OnInit {
  _intro : string[];
  _partnerships : Partnership[];
  imgFolderPath : string;

  error : boolean = false;
  load : boolean = false;

  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.imgFolderPath = 'assets/icons/partnerships/';
  }

  async loadData(){
    let data = await this.contentDataService.getPartnershipsData();
    this._intro = data["intro"];
    this._partnerships = data["partnerships"];
    this.error = data['error']; 
    this.load = true;
  }
}
