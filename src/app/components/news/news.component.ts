import { Component, OnInit } from '@angular/core';
import { ContentDataService } from '@shared/services/content-data.service';
import { News } from '@class/news';
import { compareDateDescendingOrder } from '@class/date-comparator';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  _news : News[];

  error : boolean = false;
  load : boolean = false;
  
  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(){
    let data = await this.contentDataService.getNews();
    this._news = data['newsList'].sort((a,b) => compareDateDescendingOrder(a.date, b.date));
    this.error = data['error']; 
    this.load = true;
  }

}
