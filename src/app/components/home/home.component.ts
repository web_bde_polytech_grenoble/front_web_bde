import { Component, OnInit } from '@angular/core';
import { Link, LinkType } from '@class/link';
import { ContentDataService } from '@shared/services/content-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // allow to extract data from json
  // bdeData : any = (data as any).default.bde;

  bdeData : string[];
  polytech : Link[];

  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.bdeData = [
      "Salut à toi nouveau polypote et bienvenue à Polytech Grenoble !",
      "Nous allons te montrer que tu as fait LE meilleur choix en venant dans notre école et en intégrant notre réseau ! Avec chance, tu as choisi la ville avec la meilleure ambiance et ton BDE ainsi que toutes les associations de l’école seront là pour te faire profiter pleinement de ta vie étudiante. Tu verras, la vie associative proposée à Polytech Grenoble est très riche et grâce à elle tu ne t’ennuieras jamais !",
      "L’année que tu vas vivre sera spéciale. Les conditions sanitaires actuelles nous obligent tous à fonctionner différemment.",
      "Nous savons que cette situation peut être stressante pour toi, mais ne t’inquiète pas ! Ton BDE t’a préparé 3 semaines d’intégration en aménageant, réinventant et innovant afin que tu puisses cohésionner et découvrir ce que sont l’ambiance et les valeurs de Polytech !",
      "Au-delà de la partie événementielle, le BDE sera aussi là pour t’accompagner, répondre à toutes tes questions et t’orienter si tu rencontres un quelconque problème dans l’école.",
      "Bref, tu l’as compris, nous espérons te voir très vite et je te souhaite, au nom de tous les membres du BDE, une très bonne année en notre compagnie !"
    ];

    this.polytech = [
      new Link(
          'Polytech Grenoble',
          LinkType.Facebook,
          'https://www.facebook.com/ecole.polytech.grenoble/',
          '462856110398815'
      ),
      new Link(
          'Polytech Grenoble',
          LinkType.Instagram,
          'https://www.instagram.com/ecole.polytech.grenoble/',
          'ecole.polytech.grenoble'
      )
    ];
  }

}
