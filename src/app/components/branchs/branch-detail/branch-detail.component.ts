import { Component, OnInit, Inject } from '@angular/core';
import { Branch } from '@class/branch';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-branch-detail',
  templateUrl: './branch-detail.component.html',
  styleUrls: ['./branch-detail.component.css', '../../../shared/css/dialog.css']
})
export class BranchDetailComponent implements OnInit {

  branch : Branch;
  imgFolderPath : string;

  constructor(public dialogRef: MatDialogRef<BranchDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.branch = data.branch
  }

  ngOnInit(): void {
    this.imgFolderPath = 'assets/icons/branchs/';
  }

}
