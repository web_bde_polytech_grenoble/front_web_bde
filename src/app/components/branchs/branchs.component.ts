import { Component, OnInit } from '@angular/core';
import { Branch } from '@class/branch';
import { MatDialog } from '@angular/material/dialog';
import { BranchDetailComponent } from './branch-detail/branch-detail.component'
import { ContentDataService } from '@services/content-data.service';

@Component({
  selector: 'app-branchs',
  templateUrl: './branchs.component.html',
  styleUrls: ['./branchs.component.css' , '../../shared/css/card.css'],
})
export class BranchsComponent implements OnInit {  
  _branchs : Branch[] = [];
  imgFolderPath : string;

  error : boolean = false;
  load : boolean = false;
  
  
  constructor(
    private dialog: MatDialog,
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.imgFolderPath = 'assets/icons/branchs/';
  }

  async loadData(){
    let data = await this.contentDataService.getBranchsData();
    this._branchs = data['branchsList'];
    this.error = data['error']; 
    this.load = true;
  }

  openCard(br : Branch): void{       
    const dialogRef = this.dialog.open(BranchDetailComponent, {
      autoFocus : true,
      data : {
        branch : br
      }
    });
  }
  
}