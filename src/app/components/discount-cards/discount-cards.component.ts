import { Component, OnInit } from '@angular/core';
import { DiscountCard } from '@class/discount-card';
import { ContentDataService } from '@shared/services/content-data.service';

@Component({
  selector: 'app-discount-cards',
  templateUrl: './discount-cards.component.html',
  styleUrls: ['./discount-cards.component.css', '../../shared/css/card.css']
})
export class DiscountCardsComponent implements OnInit {
  _discountCards : DiscountCard[] = [];
  imgFolderPath : string;

  error : boolean = false;
  load : boolean = false;

  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.imgFolderPath = 'assets/icons/discount_card/';
  }

  async loadData(){
    let data = await this.contentDataService.getDiscountCardsData();
    this._discountCards = data['discountCardsList'];
    this.error = data['error']; 
    this.load = true;
  }
}
