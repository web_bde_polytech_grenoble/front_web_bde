import { Component, OnInit } from '@angular/core';
import { Shortcut } from '@class/shortcut';
import { ContentDataService } from '@shared/services/content-data.service';

@Component({
  selector: 'app-shortcut',
  templateUrl: './shortcut.component.html',
  styleUrls: ['./shortcut.component.css']
})
export class ShortcutComponent implements OnInit {
  _shortcutCommun : Shortcut[];
  _shortcutTis : Shortcut[];
  _shortcutInfo : Shortcut[];

  error : boolean = false;
  load : boolean = false;

  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(){
    let data = await this.contentDataService.getShortcutsData();
    this._shortcutCommun = data["shortcutCommon"];
    this._shortcutTis = data["shortcutTIS"];
    this._shortcutInfo = data["shortcutINFO"];
    this.error = data['error']; 
    this.load = true;
  }

  openCard(link : string): void{       
    window.open(link, '_blank');
  }
}
