import { Component, OnInit } from '@angular/core';
import { Event } from '@class/event';
import { ContentDataService } from '@shared/services/content-data.service';
import { compareDateAscendingOrder } from '@class/date-comparator';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css', '../../shared/css/card.css']
})
export class EventsComponent implements OnInit {
  _eventsGre : Event[];
  _eventsReseau : Event[];
  imgFolderPath : string;

  error : boolean = false;
  load : boolean = false;

  constructor(
    private contentDataService : ContentDataService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.imgFolderPath = 'assets/icons/events/';
  }

  async loadData(){
    let data = await this.contentDataService.getEventsData();
    this._eventsGre = data["eventsGrenoble"].sort((a,b) => compareDateAscendingOrder(a.date, b.date));
    this._eventsReseau = data["eventsReseau"].sort((a,b) => compareDateAscendingOrder(a.date, b.date));
    this.error = data['error']; 
    this.load = true;
  }
}
