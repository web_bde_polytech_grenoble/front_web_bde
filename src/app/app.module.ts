import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule} from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared.module';
import { AgendaModule } from './components/agenda/agenda.module';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { ShortcutComponent } from './components/shortcut/shortcut.component';
import { NewsComponent } from './components/news/news.component';
import { AssociationsComponent } from './components/associations/associations.component';
import { HomeComponent } from './components/home/home.component';
import { BranchsComponent } from './components/branchs/branchs.component';
import { ContactComponent } from './components/contact/contact.component';
import { EventsComponent } from './components/events/events.component';
import { SongsComponent } from './components/songs/songs.component';
import { TheCityComponent } from './components/the-city/the-city.component';
import { PartnershipsComponent } from './components/partnerships/partnerships.component';
import { DiscountCardsComponent } from './components/discount-cards/discount-cards.component';
import { AssociationDetailComponent } from './components/associations/association-detail/association-detail.component';
import { BranchDetailComponent } from './components/branchs/branch-detail/branch-detail.component';
import { SongDetailComponent } from './components/songs/song-detail/song-detail.component';
import { MainPageComponent } from './pages/main-page/main-page.component';

import localeFr from '@angular/common/locales/fr';
import { registerLocaleData} from '@angular/common';
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    ShortcutComponent,
    NewsComponent,
    AssociationsComponent,
    HomeComponent,
    BranchsComponent,
    ContactComponent,
    EventsComponent,
    SongsComponent,
    TheCityComponent,
    PartnershipsComponent,
    DiscountCardsComponent,
    AssociationDetailComponent,
    BranchDetailComponent,
    SongDetailComponent,
    MainPageComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot([]),
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    AgendaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
