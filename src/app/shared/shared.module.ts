import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FollowUsComponent } from './components/follow-us/follow-us.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { DeepLinkService } from './services/deep-link.service';
import { WebStorageService } from './services/web-storage.service';
import { ContentDataService } from './services/content-data.service';
import { WebStorageModule } from 'ngx-store';

@NgModule({
  imports: [
    CommonModule,
    WebStorageModule,
  ],
  declarations: [
    FollowUsComponent,
    PageNotFoundComponent
  ],
  exports: [
    FollowUsComponent,
    PageNotFoundComponent
  ],
  providers: [
    DeepLinkService,
    WebStorageService,
    ContentDataService
  ]
})
export class SharedModule { }
