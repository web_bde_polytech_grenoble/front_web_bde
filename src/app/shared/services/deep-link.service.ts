import { Injectable } from '@angular/core';
import { Link, LinkType } from '@class/link'; 

@Injectable({
  providedIn: 'root'
})
export class DeepLinkService {

  constructor() { }

  public deepLink(snw : Link){
    let ua = navigator.userAgent.toLowerCase();
    let isAndroid = ua.indexOf("android") > -1; // android check
    let isIphone = ua.indexOf("iphone") > -1; // ios check

    if (isAndroid || isIphone){
      this.openApp(snw);
    }else{
      this.openWebPage(snw.webPath);
    }
  }

  private openWebPage(path : string) {
    let win = window.open(path, '_blank');
  }

  private getPrefix(type : LinkType){
    switch(type){
      case LinkType.Facebook:
        return 'fb://page/';
      case LinkType.Instagram:
        return 'instagram://user?username=';
      case LinkType.Mail:
        return 'mailto:';
      case LinkType.Linkedin:
        // return 'linkedin://#profile/';
        return 'linkedin://';
      case LinkType.Youtube:
        return 'vnd.youtube:';
      case LinkType.Twitter:
        return 'twitter://user?screen_name=';
      case LinkType.Telegram:
      default: // NONE, WebSite
        return ;
    }
  }
  
  private openApp(link : Link) {
    let prefix = this.getPrefix(link.type);

    if (prefix){
      let win = window.open(prefix + link.appPath);
      if(!win || win.location.protocol == 'about:'){
        win.location.href = link.webPath;
      }
    }else{
      this.openWebPage(link.webPath);
    }
  }
}
