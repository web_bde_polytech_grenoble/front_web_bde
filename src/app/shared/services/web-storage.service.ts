import { Injectable, Inject } from '@angular/core';
import { LocalStorageService,  } from 'ngx-store';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  constructor(
    private storage : LocalStorageService
    ) { }

  public setLocalStorage(key : string, value){
    this.storage.set(key, value);
  }

  public getLocalStrorage(key : string){
    return this.storage.get(key);
  }

  public removeLocalStorage(key : string){
    this.storage.remove(key);
  }
}
