import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Association } from '@class/association';
import { Branch } from '@class/branch';
import { DiscountCard } from '@class/discount-card';
import { Event } from '@class/event';
import { Link, LinkType } from '@class/link';
import { News } from '@class/news';
import { Partnership } from '@class/partnership';
import { Shortcut, ShortcutType } from '@class/shortcut';
import { Song } from '@class/song';

import { plainToClass } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class ContentDataService {
  api : string;

  constructor(
    private httpClient: HttpClient
  ) {
    this.api = environment.api;
  }

  async getAssociationsData() {
    let error : boolean = false;
    let associationsList : Association[] = [];
    let cellulesIndependantes : Association[] = [];
    
    try{
      let response = await this.httpClient.get(this.api + '/associations').toPromise();
      let ret : Association[] = plainToClass(Association, response["data"] as Association[]);
      associationsList = ret.filter(association => association.shortName == "BDE" || association.shortName == "BDS" || association.shortName == "BDA")
      cellulesIndependantes = ret.filter(association => association.shortName != "BDE" && association.shortName != "BDS" && association.shortName != "BDA")
    }catch(e){
      error = true;
    }

    return {
      "associationsList": associationsList,
      "cellulesIndependantes": cellulesIndependantes,
      "error" : error
    };
  }

  async getBranchsData(){
    let ret : Branch[] = [];
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/branchs').toPromise();
      ret = plainToClass(Branch,(response["data"] as Branch[]));
    }catch(e){
      error = true;
    }
    
    return {
      "branchsList" : ret,
      "error" : error
    };
  }

  async getDiscountCardsData(){
    let ret : DiscountCard[] = [];
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/discountcards').toPromise();
      ret = plainToClass(DiscountCard, (response["data"] as DiscountCard[]));
    }catch(e){
      error = true;
    }

    return {
      "discountCardsList" : ret,
      "error" : error
    }
  }

  async getEventsData(){
    let error : boolean = false;
    let events : Event[] = [];
    let eventsReseau : Event[] = [];

    try{
      let response = await this.httpClient.get(this.api + '/events').toPromise();
      let ret : Event[] = plainToClass(Event, response["data"] as Event[]);
      events = ret.filter(event => event.network == false || event.place == 'Grenoble');
      eventsReseau = ret.filter(event => event.network == true);
    }catch(e){
      error = true;
    }

    return {
      "eventsGrenoble" : events,
      "eventsReseau" : eventsReseau,
      "error" : error
    }
  }

  async getPartnershipsData(){
    let ret : Partnership[] = [];
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/partnerships').toPromise();
      ret = plainToClass(Partnership, (response["data"] as Partnership[]));
    }catch(e){
      error = true;
    }

    return {
      "partnerships" : ret,
      "error" : error
    }
  }

  async getShortcutsData(){
    let error : boolean = false;
    let shortcutCommun : Shortcut[] = [];
    let shortcutTIS : Shortcut[] = [];
    let shortcutINFO : Shortcut[] = [];

    try{
      let response = await this.httpClient.get(this.api + '/shortcuts').toPromise();
      let ret : Shortcut[] = plainToClass(Shortcut, (response["data"] as Shortcut[]));
      shortcutCommun = ret.filter(shortcut => shortcut.branch == "All");
      shortcutTIS = ret.filter(shortcut => shortcut.branch == "TIS");
      shortcutINFO = ret.filter(shortcut => shortcut.branch == "Info");
    }catch(e){
      error = true;
    }
    
    return {
      "shortcutCommon" : shortcutCommun,
      "shortcutTIS" : shortcutTIS,
      "shortcutINFO" : shortcutINFO,
      "error" : error
    }
  }

  async getSongsData(){
    let ret : Song[] = [];
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/songs').toPromise();
      ret = plainToClass(Song, (response["data"] as Song[]));
    }catch(e){
      error = true;
    }

    return {
      "songsList" : ret,
      "error" : error
    }
  }

  async getNews(){
    let ret : News[] = [];
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/news').toPromise();
      ret = plainToClass(News, (response["data"] as News[]));
    }catch(e){
      error = true;
    }

    return {
      "newsList" : ret,
      "error" : error
    }
  }

  async getAgendaEnglishGroup(){
    let ret : [] = [];
    let error : boolean = false;
    try{
      let response = await this.httpClient.get(this.api + '/agenda/english').toPromise();
      ret = response["data"];
    }catch(e){
      error = true;
    }

    return {
      "englishGroups" : ret,
      "error" : error
    }
  }

  async getAgendaBranchs(){
    let ret : [] = [];
    let error : boolean = false;
    try{
      let response = await this.httpClient.get(this.api + '/agenda/branchs').toPromise();
      ret = response["data"];
    }catch(e){
      error = true;
    }

    return {
      "branchsList" : ret,
      "error" : error
    }
  }

  async getAgenda(ids : string[]){
    let ret : Object[] = [];
    
    let error : boolean = false;

    try{
      let response = await this.httpClient.get(this.api + '/agenda/ids/' + ids.toString()).toPromise();
      ret = response["data"];
      error = response["error"];      
    }catch(e){
      error = true;
    }

    return {
      "eventList" : ret,
      "error" : error
    }
  }

  async getAgendaInfo(ids : string[]){
    let ret : Object[] = [];
    let error : boolean = false;

    try {
      await ids.forEach( async(value) => {
        let response = await this.httpClient.get(this.api + '/agenda/id/' + value);
        ret.push(response["data"]);
      });
    }catch(err){
      error = true;
    }

    return {
      "agendaInfo" : ret,
    }
  }
}
