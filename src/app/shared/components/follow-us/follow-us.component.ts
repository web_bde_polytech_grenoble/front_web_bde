import { Component, OnInit, Input } from '@angular/core';
import {Link, LinkType } from '@class/link';
import { DeepLinkService } from '../../services/deep-link.service';

@Component({
  selector: 'app-follow-us',
  templateUrl: './follow-us.component.html',
  styleUrls: ['./follow-us.component.css']
})
export class FollowUsComponent implements OnInit {
  // Icons need to be on public folder
  imgFb = "assets/icons/social_network/facebook.png";
  imgInsta = "assets/icons/social_network/instagram.png";
  imgSnap = "assets/icons/social_network/snapchat.png";
  imgTelegam = "assets/icons/social_network/telegram.png";
  imgLinkedin = "assets/icons/social_network/linkedin.png";
  imgMail = "assets/icons/social_network/mail.png";
  imgSiteWeb = "assets/icons/social_network/site-web.png";

  eLinkType = LinkType;
  
  @Input() _links : Link[];

  constructor(private deepLink : DeepLinkService) { }

  open(link : Link){
    this.deepLink.deepLink(link);
  }

  ngOnInit(): void {
  }

}
