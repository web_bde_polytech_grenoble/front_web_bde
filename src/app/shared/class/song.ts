import { Link } from '@class/link';

export class Song {
    constructor(
        public name : string,
        public lyrics : string[],
        public link : Link
    ){};
}