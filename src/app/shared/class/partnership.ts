import { Link } from '@class/link';

export class Partnership{
    constructor(
        public name : string,
        public imgPath : string,
        public socialNetworks : Link[],
        public description : string[],
    ){}
}