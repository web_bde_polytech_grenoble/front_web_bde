import { Link } from '@class/link';

export class Association {
    constructor(
        public shortName : string,
        public fullName : string,
        public imgPath : string,
        public socialNetworks : Link[],
        public cellules : Association[],
        public description : string[],
    ){};
}
