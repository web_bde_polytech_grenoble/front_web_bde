export enum ShortcutType {
    All = "All",
    TIS = "TIS",
    Info = "Info"
};

export class Shortcut {
    constructor(
        public name : string,
        public branch : ShortcutType,
        public connection : string,
        public link : string,
        public description : string,
        public access : string
    ){};
}