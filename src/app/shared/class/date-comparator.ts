export function compareDateAscendingOrder(date1: Date, date2: Date): number
{
  if (!date1 || !date2)
    return 0;
  if (date1 > date2) {
    return 1;
  }
  if (date1 < date2) {
      return -1;
  }
  return 0;
}

export function compareDateDescendingOrder(date1: Date, date2: Date): number
{
  if (!date1 || !date2)
    return 0;
  if (date1 < date2) {
    return 1;
  }
  if (date1 > date2) {
      return -1;
  }
  return 0;
}