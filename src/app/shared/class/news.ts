export class News{
    constructor(
        public title : string,
        public date : Date,
        public description : string[],
    ){};
}