export class Event{    
    constructor(
        public name : string,
        public date : Date,
        public place : string,
        public network : boolean,
        public imgPath : string,
        public description : string[], 
    ){};
}