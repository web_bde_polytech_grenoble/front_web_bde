export enum LinkType {
    Facebook = "Facebook",
    Twitter = "Twitter",
    Instagram = "Instagram",
    Telegram = "Telegram",
    Mail = "Mail",
    Linkedin = "Linkedin",
    WebSite = "WebSite",
    Youtube = "Youtube",
    NONE = "NONE"
};
  
export class Link {
    constructor(
        public title : string,
        public type : LinkType,
        public webPath : string,
        public appPath : string
    ){};
}