export class Branch {
    constructor(
        public shortName : string,
        public fullName : string,
        public imgPath : string,
        public description : string[],
    ){};
}
