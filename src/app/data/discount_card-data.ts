import { DiscountCard } from '@class/discount-card';

export const discountCards : DiscountCard[] = [
    new DiscountCard(
        'La carte adhérent BDE',
        'carte_bde.png',
        [],
        [
            "Si tu le souhaites, tu peux adhérer à ton BDE pour la somme de 15€.",
            "Cela te permettra d’accéder à la totalité de nos partenariats et au prix adhérents des soirées pendant toute l’année. Mais en adhérents au BDE, tu adhères également à la Federp et au réseau Polytech te permettant l’accès aux évènements réseaux à des tarifs réduits !"
        ]
    ),
    new DiscountCard(
        'Emblem',
        'emblem.png',
        [],
        [
            "La carte Emblem est une carte qui te permet de bénéficier d’offres promotionnelles dans toute l’agglomération. Elle est vendue au prix de 15€ mais immédiatement et largement rentabilisée grâce aux cadeaux découvertes, offerts sans condition lors de ta première visite dans certaines enseignes. En plus des cadeaux découvertes, il y a les offres permanentes, valables toute l’année et autant de fois que tu le souhaites, dans la restauration, les bars, le loisir, le sport ainsi que dans les services.",
            "Tu pourras également accéder à des tarifs préférentiels lors d’événements tels que des festivals ou des rencontres sportives!"
        ]
    ),
    new DiscountCard(
        'CVA',
        'CVA.png',
        [],
        [
           "Il s’agit d’une carte de réduction réservée aux étudiants de Grenoble INP. Pour le même prix que la carte Emblem (15€), tu auras accès à plus d’une trentaine de partenaires. Et il y en a pour tous les goûts: bars, restaurants, loisirs…",
           "En plus des réductions dans les différents commerces partenaires, cette carte vous donne également des prix préférentiels sur tous les évènements INP ! Cette année la carte CVA prend également la forme d’un forfait de ski rechargeable en ligne pour la station de Oz-Vaujany. Il y a également la possibilité de l’avoir en dématérialisée sur l’application Grand Cercle." 
        ]
    ), 
];