import { News } from '@class/news';

export const newsList : News[] = [
    new News(
        'Application Smartphone',
        new Date(),
        [
            'Vous pouvez faire fonctionner le site comme une application smartphone.',
            'Pour cela, accéder au site depuis votre smartphone (en vous mettant sur la page que vous souhaitez voir en première). Ensuite aller dans les paramètres de votre navigateur et appuyer sur `Ajouter à l\'écran d\'accueil`.',
            'Vous pouvez maintenant utiliser le site comme une application native.'
        ]
    )
]