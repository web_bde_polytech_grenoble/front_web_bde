import { Shortcut, ShortcutType } from '@class/shortcut'

export const ShortcutCommun : Shortcut[] = [
    new Shortcut(
    'Léo', 
    ShortcutType.All,
    'Connexion via identifiants Agalan', 
    'https://leo.univ-grenoble-alpes.fr/',
    '', 
    'boîte mail UGA, ADE (emploie du temps fourni par l\'UGA, SUAPS)'
    ),
    new Shortcut(
    'Chamilo', 
    ShortcutType.All, 
    'Connexion via identifiants Agalan', 
    'https://chamilo.univ-grenoble-alpes.fr', 
    '', 
    'cours de Tronc Commun (anglais, droit/éco, gestion, ..)'
    ),
];

export const ShortcutTIS : Shortcut[] = [
    new Shortcut(
    'Moodle', 
    ShortcutType.TIS, 
    'Connexion via identifiants Agalan', 
    'https://im2ag-moodle.e.ujf-grenoble.fr/course/index.php?categoryid=50', 
    '', 
    'cours de filières TIS'
    ),
];

export const ShortcutINFO : Shortcut[] = [
    new Shortcut(
    'Moodle',  
    ShortcutType.Info,
    'Connexion via identifiants Agalan', 
    'https://im2ag-moodle.e.ujf-grenoble.fr/course/index.php?categoryid=20', 
    '', 
    'cours de filières INFO'
    ),
];