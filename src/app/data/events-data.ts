import { Event } from "@class/event";

export const events : Event[] = [
    new Event(
        'Le Chaam',
        '',
        'Grenoble',
        '',
        [
            "C'est tous ensemble que nous fêtons chaque année comme il se doit l'anniversaire de notre cher Georges Yéti lors d'une soirée à thème où Polytech Lyon et Savoie sont conviés. Pourquoi ce nom ? à toi de le découvrir..."
        ]
    ),
    new Event(
        'Le WECS',
        '',
        'Grenoble',
        '',
        [
            "ANNULE : cause Covid",
            "C'est l'occasion de te faire plein de polypotes au travers d'un week-end dans un lieu tenu secret... Au programme, de nombreuses activités plus folles les unes que les autres, des soirées inoubliables (ou pas) et une ambiance dont seule Polytech a le secret !"
        ]
    ),
    new Event(
        'Le TIF',
        '',
        'Grenoble',
        '',
        [
            "Le TIF est l’occasion pour toi de représenter ta filière lors d’un tournoi sportif ! Il y aura plusieurs sports comme handball, basket, football, pompoms etc... Si tu n’es pas sportif pas de panique encourager son équipe fait partie de la compétition grâce au concours de la filière qui met le plus d’ambiance !"
        ]
    ),
    new Event(
        'Les Ol\'INPiades',
        '',
        'Grenoble',
        '',
        [
            "Qui de Popo, de Phelma ou des autres écoles des INPs Grenoble possède les meilleurs athlètes ? Défends ton école à travers diverses épreuves sportives, admire un défilé de char digne de Rio et viens encourager les pompoms !"
        ]
    ),
    new Event(
        'Le Gala',
        '',
        'Grenoble',
        '',
        [
            "Cet événement vient couronner 5 années d’études (ou plus pour les plus téméraires) lors d'une soirée de prestige et qui marque l'entrée dans la vie active (certains ont plus de mal que d'autres à couper le cordon)."
        ]
    ),
    new Event(
        'Le Bal',
        '',
        'Grenoble',
        '',
        [
            "C’est un évènement tout nouveau à Polytech Grenoble ! C’est l’occasion de retrouver tes polypotes avec une ambiance plus classe que d’habitude. Ici pas de fourrures ou de vêtements tous sales mais plutôt belle chemise ou belle robe."
        ]
    ),
    new Event(
        'Le SONOEL',
        '',
        'Grenoble',
        '',
        [
            "Noël approche? C’est l’occasion parfaite pour taper du pied une dernière fois avant de changer d’année."
        ]
    ),
    new Event(
        'La White',
        'Février',
        'Grenoble',
        '',
        [
            "S’il y a bien un événement réseau à ne pas manquer, c’est la White ! La White c’est quoi ? C’est un événement qui se déroule sur un week-end entier où l’on t’attend tout vêtu de blanc. Ce week-end, c’est des rencontres, des retrouvailles puis des souvenirs entre polypotes, et surtout l’occasion de représenter Polytech Grenoble et montrer que nous sommes bel et bien la capitale du réseau."
        ]
    ),
]

export const eventsReseau : Event[] = [
    new Event(
        "Chaam",
        'Septembre',
        'Grenoble',
        '',
        [

        ]
    ),
    new Event(
        "Azur",
        'Octobre',
        'Marseille',
        '',
        [

        ]
    ),
    new Event(
        "Pink",
        'Octobre',
        'Annecy-Chambery',
        '',
        [

        ]
    ),
    new Event(
        "MNM'S",
        'Octobre',
        'Montpellier',
        '',
        [

        ]
    ),
    new Event(
        "Derby",
        'Octobre',
        'Paris-Saclay',
        '',
        [

        ]
    ),
    new Event(
        "Discount",
        'Novembre',
        'Clermont',
        '',
        [

        ]
    ),
    new Event(
        "Blue",
        'Novembre',
        'Montpellier',
        '',
        [

        ]
    ),
    new Event(
        "7 Filiere",
        'Novembre',
        'Clermont',
        '',
        [

        ]
    ),
    new Event(
        "Gone",
        'Décembre',
        'Lyon',
        '',
        [

        ]
    ),
    new Event(
        "TPN",
        'Janvier',
        'Clermont',
        '',
        [

        ]
    ),
    new Event(
        "White",
        'Février',
        'Grenoble',
        '',
        [

        ]
    ),
    new Event(
        "TPW",
        'Février',
        'Nantes',
        '',
        [

        ]
    ),
    new Event(
        "PNW",
        'Mars',
        '',
        '',
        [

        ]
    ),
    new Event(
        "Tigresses",
        'Mars',
        'Orléans',
        '',
        [

        ]
    ),
    new Event(
        "Gold Silver",
        'Mars',
        'Paris-Sorbonne',
        '',
        [

        ]
    ),
    new Event(
        "Poly Sound",
        'Avril',
        'Nancy',
        '',
        [

        ]
    ),
    new Event(
        "Beach",
        'Mai',
        'Montpellier',
        '',
        [

        ]
    ),
    new Event(
        "Poly'sonades",
        'Mai',
        'Tours',
        '',
        [

        ]
    ),
]