import { Partnership } from '@class/partnership';

export const partnershipIntro = [
    "Salut à toi, jeune étudiant.e de Polytech Grenoble. Savais-tu que certaines personnes de notre âge payent encore leurs bières et leurs tacos à plein tarif ? ",
    "Nous, la team partenariats, on ne trouve pas ça normal. ",
    "Comme cela est expliqué précédemment, tu peux vite devenir propriétaire d’une carte adhérent BDE Polytech Grenoble. Grâce à celle-ci, tu pourras bénéficier de tarifs préférentiels dans de nombreux commerces, restaurants et bars. Ces services te seront utiles dans la vie de tous les jours, pour tes loisirs, tes activités mais également (et surtout) pour tes soirées. Ils sont exclusifs au BDE, et seuls les adhérents pourront bénéficier de ces offres.",
    " N’oublie pas de prendre ta nouvelle carte avec toi, puisqu’elle te sera demandée lors de ton passage en caisse si tu veux profiter de ses avantages. Nous sommes constamment à la recherche de nouveaux partenaires, et tu peux nous faire part de tes idées en nous envoyant un message sur les réseaux sociaux. ",
    "Alors, soit tu nous suis, et tu commences à faire de bonnes affaires dans les commerces Grenoblois avec tes potes, soit tu restes dans ton coin, et tu payes ta consommation au même prix qu’un touriste.",
    "Pour nous, la question elle est vite répondue."
]

export const partnerships : Partnership[] = [
    new Partnership(
        'V&B',
        'vnb.png',
        [],
        [
            "Le bar ‘VnB’, à proximité de Polytech, idéal pour les afterworks. La pinte de bière allemande, brune ou blonde, en pression comme en bouteille, est à 3.5€."
        ]
    ),
    new Partnership(
        'Ange',
        'ange.png',
        [],
        [
            "La boulangerie ‘Ange’, située dans la galerie marchande du géant casino. Tu bénéficieras de 15% de réduction sur l’ensemble de ta commande."
        ]
    ),
    new Partnership(
        'Festi Dream',
        'festi_dream.png',
        [],
        [
            "La boutique ‘Festi Dream’, dans le centre-ville de Grenoble. Pour préparer tes soirées, te déguiser et réaliser des textiles personnalisés aux meilleurs prix." 
        ]
    ),
    new Partnership(
        'Naturavelo',
        'naturavelo.png',
        [],
        [
            "La boutique ‘Naturavelo’, proche de l’arrêt de tramway ‘Flandrin Valmy’. Pour préparer tes futures ballades en vélo, tu bénéficieras de 10% de réductions sur les pièces, les accessoires et les locations."
        ]
    ),
    new Partnership(
        'Chez Harry',
        'chez_harry.png',
        [],
        [
            "Le restaurant ‘Chez Harry’, a deux pas de la place Victor Hugo. Tu pourras déguster de délicieux plats de saison, faits avec des produits de qualité. Tu peux également louer des jeux vidéo et des jeux de société pour passer des soirées endiablées avec tes amis."
        ]
    ),
    new Partnership(
        'Forme altitude',
        'forme_altitude.png',
        [],
        [
            "La salle de sport ‘Forme altitude’. En plein cœur de la ville, ce centre de fitness t’accueille du lundi au samedi de 6h à 22h. Elle également ouverte le dimanche matin, si tu souhaites décuver de la soirée de la veille. Tu bénéficieras d’un tarif préférentiel lors de ton inscription grâce à ta carte BDE."
        ]
    ),
    new Partnership(
        'En Cas de Faim',
        'en_cas_de_faim.png',
        [],
        [
            "• Le restaurant ‘En Cas de Faim’. A quelques minutes du fameux PPM, ce fast-food est considéré par les protagonistes comme l’un des meilleurs de la ville. Idéal pour ne pas aller faire la fête le ventre vide lors de tes nombreuses soirées. Alors saisis toi de ta carte, et profite d’une boisson offerte pour l’achat d’un tacos."
        ]
    ),
]