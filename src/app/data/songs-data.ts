import { Song } from "@class/song";
import { Link, LinkType } from '@class/link';

export const songsList = [
    new Song(
        'Comptoir Polytech',
        [
            "Polytech Grenoble, assise au comptoir",
            "Chantez tous ensemble,",
            "Ce refrain joyeux :",
            "J’étais plein hier ce soir,",
            "Je suis plein ce soir",
            "Et si tout va bien je serais plein demain matin !",
            "Lalala lalalala",
            "Hé ! (x2)",
            "Lalalal lalalalalalalalalalalalala Hé !"
        ], 
        null
    ),
    new Song(
        'La petite Jaja',
        [
            "Pooooooooooooo... PO..LY..TECH !",
            "On.. l’aime.. bien la p’tite Jaja, Hé !",
            "Elle est si belle et si gentille",
            "On l’aime bien ! Qui ça ?",
            "La petite Jaja ! Où ça ?",
            "A Poolyytteeeeecccch",
            "JAMAIS DEUX SANS TROIS",
            "(X3)"
        ], 
        null
    ),
    new Song(
        'Jaune',
        [
            "Jaune, comme un ricard servi avant d'aller manger",
            "Jaune, avé deux trois glaçons au fond d'un verre à pied",
            "Jaune et deux volumes d'eau, je l'aime bien tassé !",
            "Jaune, avé quelques olives pour bien l'accompagner",
            "Jaune, avé deux trois copains la convivialité",
            "Jaune, pour refaire le monde quand je suis bien pété",
            "Jaune, je l'aime tellement c'est mon petit péché",
            "Jaune en trempant des croissants au petit déjeuner",
            "",
            "J'en bois à l'apéro, c'est bon c'est anisé",
            "J'en bois aussi au digeot, c'est pas bon mélangé",
            "J'en bois entre les repas pour me désaltérer",
            "Je vide les bouteilles parfois c'est abusé",
            "",
            "Jaune, comme le feu des gitans autour des poulaillers",
            "Jaune, comme le maillot d'Indurain sur les Champs-Élysées",
            "Jaune, comme l'auto de la poste qu'amène le courrier",
            "Jaune, comme le blanc de mes yeux quand je suis défoncé",
            "",
            "Refrain",
            "(Reprise au début)",
            "",
            "J'en bois à l'apéro, c'est bon c'est anisé",
            "LALALLALALALLALALALLALALALLALA",
            "Je vide les bouteilles parfois c'est abusé"
        ], 
        new Link(
            'Jaune 34alain34',
            LinkType.Youtube,
            'https://www.youtube.com/watch?v=wNch1OqYgIM',
            'wNch1OqYgIM'
        )
    ),
    new Song(
        'La chasse à l`ours',
        [
            "On part à la chasse à l’ours (bis)",
            "On arrive devant une montagne (bis)",
            "On peut pas passer de ce côté (bis)",
            "Alors il faut grimper ! (bis)",
            "",
            "Ouhouhouh",
            "",
            "On part à la chasse à l’ours (bis)",
            "On arrive devant une rivière (bis)",
            "On peut pas passer de ce côté (bis),",
            "On peut pas passer de l’autre côté (bis)",
            "Alors il faut nager ! (bis)",
            "",
            "Ouhouhouh",
            "",
            "On part à la chasse à l’ours (bis)",
            "On arrive devant polytech (bis)",
            " Y’a des copains de ce côté (bis),",
            " Y’a des copains de l’autre côté (bis)",
            "Alors il faut s’ambiancer ! (bis)",
            "",
            "Ouhouhouh",
            "",
        ], 
        null
    ),
    new Song(
        'Les bidons d`eau',
        [
            "(Ici, tout est une histoire de rythme, l\'ordre des chiffres peut être modifié, on prend vite le coup de main)",
            "",
            "1 bidon 2 bidons 3 bidons d'eau,",
            "4 bidons d'eau, 5 bidons d'eau,",
            "6 bidons 7 bidons 8 bidons d'eau,",
            "9 et 10 bidons d'eau.",
            "À toi ! ",
            "",
            "*désigner quelqu’un et s’il échoue :*",
            "Il est complètement ivre,",
            "il a pas su nous dire",
            "*et c’est reparti pour un tour*"
        ], 
        null
    ),
    new Song(
        'Assis debout',
        [
            "Assis on est pas debout",
            "*on se lève*",
            "Debout on n’est pas assis",
            "*on s’assoit*",
            "",
            "(à répéter autant de fois que nécessaire)"
        ], 
        null
    ),
    new Song(
        'Du bon fromage',
        [
            "*Quelqu\'un dit quelque chose qui finit par -age*",
            "Mais (le mot finissant en -age),",
            "ça rime avec fromage !",
            "Tiens voilà du bon fromage",
            "Du bon fromage avec du lait",
            "Et celui qui l\'a fait, Qui ça? (bis)",
            "Et celui qui l\'a fait",
            "il vient de mon village."
        ], 
        null
    ),
    new Song(
        'Il y avait des crocodiles',
        [
            "Y avait des cro-crocodiles et des orang-outans.",
            "Des affreux reptiles, des jolis moutons blancs.",
            "Y avait des quoi ? Des chats !",
            "Des quoi ? Des rats, des éléphants.",
            "Il ne manquait personne,",
            "pas même la lionne et la jolie licorne."
        ], 
        null
    ),
]