//////////////////////////////
// FILE OF GLABALE VARAIBLE //
//////////////////////////////
export const TextVariable = {
    SiteName: 'BDE Polytech Grenoble',
    Home: 'Accueil',
    School: 'L\'école',
    Association: 'Les Associations',
    Branch: 'Les Filières',
    Agenda: 'EDT',
    Shortcut: 'Raccourcis',
    News: 'Les Actualités',
    Contact: 'Contact'
}