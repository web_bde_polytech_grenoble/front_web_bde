import { Association } from '@class/association'
import { Link, LinkType } from '@class/link'

export const BDECellules : Association[] = [
    new Association(
        'BDP', 
        'Bureau des PeiP', 
        'BDE/BDP.jpg', 
        [
            new Link(
                'BDP Polytech Grenoble',
                LinkType.Instagram,
                'https://www.instagram.com/bdpolytech_grenoble/',
                'bdpolytech_grenoble'
            )
        ], 
        [], 
        [
            'Le bureau des PeiP (BDP) est une cellule du BDE ayant pour but de vous accompagner dans votre intégration et dans votre parcours tout au long de l\'année.', 
            'Nous avons comme projet de vous faire découvrir Grenoble, sa ville, ses lieux symboliques et son école Polytech.', 
            'Nous serons là pour vous organiser des événements en PeiP1 et PeiP2, afin que votre année se passe le mieux possible, dans la bonne humeur et la bonne entente.'
        ]
    ),
    new Association(
        'BDH',
        'Cellule Humani\'Terre',
        'BDE/BDH.png', 
        [
            new Link(
                'BDP Polytech Grenoble',
                LinkType.Facebook,
                'https://www.facebook.com/humanitas.polytech.grenoble/',
                '500100246779186'
                ),
            new Link(
                'BDP Polytech Grenoble',
                LinkType.Instagram,
                'https://www.instagram.com/bdhpolytechgrenoble/',
                'bdhpolytechgrenoble'
            )
        ],
        [],
        [
            'Coucou les polypotes ! Ici le BDH aka, le Bureau De l’Humani’terre, l’asso solidaire et écolo de Polytech !', 
            'Avec nous, et selon vos envies et vos disponibilités, vous aurez l’occasion de participer à la distribution de paniers de fruits et légumes bio aux étudiants, d’apprendre les bases de l’apiculture en prenant part à l’installation d’abeilles sur le toit de l’école, et de vous occuper de la serre aquaponique connectée. Mais cette année, nous vous réservons aussi quelques nouveautés !', 
            'Nous vous proposerons notamment des ateliers DIY (Do It Yourself), des Clean Walks, des collectes caritatives, des conférences, et plein d’autres surprises !', 
            'Alors que vous ayez la fibre écolo ou non, que vous soyez philanthropes ou non, n’hésitez pas à venir discuter avec nous, le BDH, c’est avant tout du partage, de la découverte, et de la bonne humeur !'
        ]
    )
];

export const BDSCellules = [
    new Association(
        'BDSM',
        'Bureau des Sport de Montagne',
        'BDS/BDSM.jpg',
        [
            new Link(
                'BDSM',
                LinkType.Facebook,
                'https://www.facebook.com/BureauSportDeMontagne.PolytechGre/',
                '403018976879754'
                ),
            new Link(
                'BDSM',
                LinkType.Instagram,
                'https://www.instagram.com/polymontagnards_grenoble/',
                'polymontagnards_grenoble'
            )
        ],
        [],
        [
            'Grenoble étant entourée de Montagnes, échapper à leur vision te sera quasiment impossible. Si tu désires aller au contact de celles-ci pour t’éclater et découvrir le bassin Grenoblois d’en haut, le BDSM est là pour toi ! ', 
            'Notre cellule encourage celles et ceux qui sont partants pour prendre de l’altitude et leur permet de se lancer avec du prêt de matériel. De l’escalade au VTT, en passant par le canyoning, le ski et les randonnées à pied ou en raquettes, sans oublier apéros et raclettes, tu trouveras certainement ton bonheur et des camarades avec qui le partager.', 
            'Alors si tu veux un club qui claque, n’hésite plus et rejoins-nous !'
        ]
    ),
    new Association(
        'PomPom',
        '',
        'BDS/pompoms.png',
        [
            new Link(
                'PomPom',
                LinkType.Facebook,
                'https://www.facebook.com/PompomPolytechGrenoble/',
                '365502223566936'
            ),
            new Link(
                'PomPom',
                LinkType.Instagram,
                'https://www.instagram.com/pompom_polytechgrenoble/',
                'pompom_polytechgrenoble'
            )
        ],
        [],
        [
            'Envie de représenter ton école ? Dans ce cas, les Pompoms de Polytech sont là pour toi ! ', 
            'Quelque soit ton niveau, pro ou même toi, jeune polypote qui n’a jamais réussi à danser en rythme, ce club est fait pour toi ! Aucune sélection ni aucun jugement, tu pourras venir rencontrer du monde et te défouler sur des chorées tous niveaux.', 
            'Nous organisons plusieurs représentations au cours de l’année : Ol’INPiades, remise des diplômes, journée portes ouvertes … et beaucoup d’autres !', 
            'Raison de plus pour te convaincre : tes efforts en tant que danseur pourront être récompensés par des Polypoints selon l’évènement ! Alors ne perds pas de temps et rejoins nous !', 
            'Les entraînements se déroulent en Salle 105 de 17h45 à 19h. Pour plus d’infos, n’hésite pas à nous suivre sur nos différentes pages !', 
            'Aucune obligation ni impératif !', 'On espère te voir très vite avec nous !'
        ]
    )
];

export const BDACellules = [
    new Association(
        'Polyband',
        '',
        'BDA/polyband.png',
        [
            new Link(
                'PolyBand',
                LinkType.Facebook,
                'https://www.facebook.com/Polyband-BDA-Polytech-Grenoble-114697373596248/',
                '114697373596248'
            )
        ],
        [],
        [
            'La musique et le chant résonnent bien chez toi ? Viens donc faire un tour au local Polyband, tu y trouveras toujours des passionnés !', 
            'Tu n’as pas touché à ton instrument depuis des siècles ou tes cordes vocales sont rouillées ? N’aie crainte, nous t’accueillerons avec plaisir ! Si tu débutes mais que tu es curieux, des zikos pourront te donner les bases. Tous les instruments et styles sont les bienvenus, place à la diversité !', 
            'On organise des concerts, soirées et autres projets qui te seront dévoilés au cours de l’année ! Alors si tu veux plus d’infos, n’hésite pas à suivre Polyband sur Facebook !'
        ]
    ),
    new Association(
        'PolyBeats',
        '',
        'BDA/polybeatz.png',
        [
            new Link(
                'PolyBeats',
                LinkType.Facebook,
                'https://www.facebook.com/polybeatzmusic/',
                '1865502663673696'
            )
        ],
        [],
        [
            'Salut à toi jeune polypote ! Est-ce que tu veux devenir le roi de la scène et amener tous tes polypotes au summum du caisson avec des sets de qualité, dignes de Tomorrowland ?', 
            'JE PENSE QUE LA QUESTION, ELLE EST VITE RÉPONDUE !', 
            'Que tu sois passionné.e de musique électro ou un maître des platines, PolyBeatz est fait pour toi. Grâce à des apprentissages de mix et de soirées, tu deviendras (sûrement) une star du Rézo. Tu pourras enfin augmenter ton sex-appeal, ou tout simplement, partager avec nous ta passion pour l’EDM. Rejoins-nous pour devenir le prochain Martin Garrix de Polytech !'
        ]
    ),
    new Association(
        'La Palette',
        '',
        'BDA/Palette.png',
        [
            new Link(
                'La Palette',
                LinkType.Facebook,
                'https://www.facebook.com/LaPaletteBDAPolytechGrenoble/',
                '106762691061430'
            )
        ],
        [],
        [
            'La Palette est la référence pour tous les dessinateurs !', 
            'Si tu es déjà confirmé.e, si tu n’as plus dessiné depuis le collège mais que tu veux t’y remettre, si tu veux simplement apprendre, ou juste passer du bon temps, eh bien notre club est fait pour toi !', 
            'En nous retrouvant une heure par semaine, tu pourras partager tes tips, discuter et nous impressionner avec tes créations.', 
            'Si tu n’as pas envie de prendre tout ton matériel ou que tu n’en as pas, ne t’inquiète pas ! Nous avons des feuilles et feutres pour que tu puisses laisser ton imagination te guider. Alors qu’est-ce que tu attends ?'
        ]
    ),
    new Association(
        'OP',
        'Objectif Polytech','BDA/OP.png',
        [
            new Link(
                'OP',
                LinkType.Facebook,
                'https://www.facebook.com/objectifpolytech/',
                '675827702552585'
            )
        ],
        [],
        [
            'Que tu sois débutant.e, aguerri.e dans la photographie, avec ou sans matériel, Objectif Polytech (OP) t’ouvre ses portes.', 
            'OP a pour but d’immortaliser tous les bons moments que tu vivras au sein de Polytech Grenoble. ', 
            'Entre sorties découvertes, concours photos et pleins d’autres activités liées à l’univers de la photographie, tu pourras t’épanouir et trouver ce qui te convient.', 
            'Tu seras amené.e à retrouver ton club dans le groupe privé l’Apparezo où les photos de tous les événements réseaux des écoles Polytech sont publiées.', 
            'N’hésite pas à contacter le club grâce à la page Facebook !'
        ]
    ),
    new Association(
        'Club Ciné',
        '',
        'BDA/club_ciné.png',
        [
            new Link(
                'Club Ciné',
                LinkType.Facebook,
                'https://www.facebook.com/CineClubPolytech/',
                '106005314469196'
            )
        ],
        [],
        [
            'Besoin de respirer l’air pur du Gondor ? Envie de faire des châteaux de sable sur Tatooïne ?', 
            'Mieux qu’une agence de voyage, le Club Ciné te fait une offre que tu ne pourras pas refuser : t’échapper de la routine grâce à des projections de films toutes les 2 semaines.', 
            'Et pas besoin d’être un expert : que tu tutoies quotidiennement Brad Pitt ou que tu ne connaisses pas Spielberg, tu es le ou la bienvenu.e !', 
            'Alors ramène ton pop-corn et tes polypotes, et viens découvrir avec nous les plus grandes œuvres du 7ème art comme les OFNI les plus nanardesques.'
        ]
    ),
    new Association(
        'Polythéâtre',
        '',
        'BDA/polytheatre.png',
        [
            new Link(
                'Polythéâtre',
                LinkType.Facebook,
                'https://www.facebook.com/Club-Théâtre-Polytech-Grenoble-104052318004271/',
                '104052318004271'
            )
        ],
        [],
        [
            'Salut les polypotes ! Ici la cellule théâtre avec Farès et Emilie, vos respos Polythéâtre, toujours là pour vous servir !', 
            'Que tu sois timide, débutant.e ou aguerri.e, si tu souhaites faire du théâtre pour découvrir ou te délier la langue, tu seras bienvenu.e parmi nous. Tu te trouveras peut-être un talent caché !', 
            'Le sérieux est de mise lors des séances mais cela ne nous empêche nullement de rire. Personne n’est là pour juger, alors lâche-toi, éclate-toi et montre-nous tes plus belles imitations ou tes meilleures improvisations ! En tout cas, on a hâte de te voir !'
        ]
    ),
    new Association(
        'Polycook',
        '',
        'BDA/polycook.png',
        [
            new Link(
                'Polycook',
                LinkType.Facebook,
                'https://www.facebook.com/PolyCook-La-page-des-cuistos-102879008117935/',
                '102879008117935'
            )
        ],
        [],
        [
            'Es-tu passionné.e de cuisine ? Adores-tu les pâtes au sucre ?', 
            'REJOINS DONC POLYCOOK ! Le club de cuisine où tu pourras t’épanouir autour d’un maximum nourriture.', 
            'Entouré des meilleurs chefs de l’école, tu vas pouvoir apprendre à briller à toutes les soirées grâce à tes bons petits plats. Que tu sois team Philippe Etchebest ou team chef de cauchemar en cuisine, tu es bienvenu.e parmi nous !', 
            'Au programme : recettes, dégustations & cours de cuisine pour faire de toi l’élite de la nation culinaire. N’attends plus et rejoins-nous sur Polycook - la page des cuistos !'
        ]
    ),
    new Association(
        'Sonorock',
        '',
        'BDA/sonorock.png',
        [
            new Link(
                'Sonorock',
                LinkType.Facebook,
                'https://www.facebook.com/SonoRock-Polytech-Grenoble-104064881333841/',
                '104064881333841'
            )
        ],
        [],
        [
            'Salut toi qui débarque à Grenoble ! Bienvenue dans cette super école qui, j’espère, sera à la hauteur de tes attentes.', 
            'Ici nous te présentons SonoRock, un club faisant partie du BDA qui est là pour t’apprendre à danser le rock en binôme, et ça toute l’année !', 
            'C’est un club tout nouveau qui espère te compter parmi ses membres. Que tu sois débutant ou amateur, seul ou accompagné, tu es le bienvenu pour apprendre à nos côtés. Tu auras l’occasion de te représenter aux évènements importants de l’école afin de les animer. Si tu as des questions, tes deux respos sont là !'
        ]
    ),
    new Association(
        'BDJ',
        'Bazar des jeux',
        'BDA/BDJ.png',
        [
            new Link(
                'BDJ',
                LinkType.Facebook,
                'https://facebook.com/BazarDesJeux/',
                '1509220166068328'
            )
        ],
        [],
        [
            'Salutations jeune tueur de gobelin ! Ou plutôt... fan de jeux de société ? Ah non, je sais ! Tu préfères te défouler sur Smash Bros ou dans la Faille de l’Invocateur ?', 
            'Que tu aies compris chaque mot ci-dessus ou que tu sois juste un.e adepte du Monopoly, bienvenue au Bazar Des Jeux !', 
            'Ici on joue, on clique, on parle, on lance des dés et surtout on s’amuse !', 
            'Bien qu’on aime Pandemic, avec le COVID19 on va privilégier les évènements en ligne, mais il y aura quand même des après-midi jeux et des soirées à thème. Alors si tu veux participer à l’aventure, découvrir de nouveaux jeux et rencontrer des compagnons, rejoins-nous vite !'
        ]
    ),
    new Association(
        'Pop\'Otaku',
        '',
        'BDA/pop_otaku.png',
        [
            new Link(
                'Pop\'Otaku',
                LinkType.Facebook,
                'https://www.facebook.com/ClubPopOtaku/',
                '103991448005578'
            )
        ],
        [],
        [
            'Konichiwa Mina San!', 
            'Pop’Otaku est le club destiné aux amateurs de culture japonaise. Ici tu pourras découvrir et redécouvrir tes animes préférés grâce à des diffusions régulières dans une salle cinéma rien qu’à nous. ', 
            'En plus, tu pourras participer à des séances de karaoké où tu pourras chanter à tout cœur, et si cela t’intéresse, nous allons aussi organiser des concours de cosplay au cours de l’année !', 
            'Alors si tu es fan du Japon mais que tu n’as personne avec qui en discuter, Pop’Otaku est fait pour toi et te permettra de rencontrer plein de nouveaux polypotes.', 
            'Nous t’attendons avec impatience !'
        ]
    )
];

export const AssociationsList = [
    new Association(
        'BDE',
        'Bureau des Eleves',
        'BDE/BDE.png',
        [
            new Link(
                'BDE Polytech Grenoble',
                LinkType.Facebook, 
                'https://www.facebook.com/bde.polytechgre/',
                '459140794107012'
            ),
            new Link(
                'Petites annonces de Polytech Grenoble',
                LinkType.Facebook, 
                'https://www.facebook.com/groups/625320278049481/',
                '625320278049481'
            ),
            new Link(
                'BDE Polytech Grenoble',
                LinkType.Instagram,
                'https://www.instagram.com/bde_polytech_grenoble/',
                'bde_polytech_grenoble'
            )
        ],
        BDECellules,
        [
            'Le BDE (Bureau Des Élèves) est une association de 55 étudiants motivés et volontaires, élue pour te faire passer une année de folie et ainsi te créer de superbes souvenirs inoubliables.', 
            'Si tu es adhérent au BDE, tu auras accès à nos événements à un tarif préférentiel et pourras également bénéficier de nombreuses réductions dans certains commerces grâce à nos partenariats.', 
            'En tout cas, si tu as le moindre problème ou juste une envie de nous parler, n’hésite pas, nous sommes là !'
        ]
    ),
    new Association(
        'BDS',
        'Bureau des Sport',
        'BDS/BDS.png', 
        [
            new Link(
                'BDS',
                LinkType.Facebook,
                'https://www.facebook.com/bds.polytechgre/',
                '791396120881543'
            ),
            new Link(
                'BDS',
                LinkType.Instagram,
                'https://www.instagram.com/bds_polytech_grenoble/',
                'bds_polytech_grenoble'
            )
        ],
        BDSCellules,
        [
            'Tu emménages à Grenoble pour tes études à Polytech Grenoble, mais tu n’as pas envie d’arrêter ton sport favori ?! Tu as envie de découvrir un nouveau sport dans la bonne ambiance et avec tes nouveaux Polypotes ?!',
            'C’est auprès du BDS qu’il faut te tourner !', 
            'Il te propose tous les sports collectifs les plus populaires, ainsi que des nouveautés telles que le street workout ou un groupe running !', 
            'Tous les niveaux sont acceptés, du débutant au confirmé, pour varier au maximum les plaisirs et s’entraider.', 
            'Des événements sportifs t’attendent (on l’espère, merci covid) au long de l’année ! Alors rejoins nous et viens défendre (si tu le souhaites) les couleurs de ta filière ou de ton école au cours de ces événements.'
        ]
    ),
    new Association(
        'BDA',
        'Bureau des Arts',
        'BDA/BDA.png', 
        [
            new Link(
                'BDA',
                LinkType.Facebook,
                'https://www.facebook.com/BDAPolytechGrenoble/',
                '374474909370070'
            ),
            new Link(
                'BDA',
                LinkType.Instagram,
                'https://www.instagram.com/bda_polytechgrenoble/',
                'bda_polytechgrenoble'
            )
        ],
        BDACellules,
        [
            'Si tu cherches une association à rejoindre pour t’amuser et te faire des polypotes, le BDA est là pour toi !', 
            'En effet, le Bureau Des Activités est prêt à t’accueillir et à te proposer pleins d\'activités pour bien t’occuper tout au long de l’année, à l’aide de notre mascotte Freddy le Colibri qui sera toujours là pour répondre à tes questions et te partager les meilleurs événements du moment.', 
            'Que tu sois passionné.e par la musique, la peinture, l’audiovisuel ou même la cuisine, tu trouveras ta place dans un (ou plusieurs) de nos clubs.', 
            'Tu ne nous crois pas ? On te laisse prendre connaissance de nos dix clubs par la suite !'
        ]
    ),
];

export const CellulesIndependantes = [
    new Association(
        'APoG', 
        'Association des anciens de polytech grenoble', 
        'indep/asso_inge.png', 
        [
            new Link(
                'APoG',
                LinkType.Facebook,
                'https://www.facebook.com/apog.page/', 
                '551568328281881'
            ),
            new Link(
                'APoG',
                LinkType.Linkedin,
                'https://www.linkedin.com/company/association-des-anciens-de-polytech-grenoble-apog',
                'company/association-des-anciens-de-polytech-grenoble-apog?trk=vw_smart_app_banner'
            ),
            new Link(
                'APoG - Groupe des diplômé.e.s de Polytech Grenoble',
                LinkType.Linkedin,
                'https://www.linkedin.com/signup/cold-join?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fgroups%2F3631%2F&trk=login_reg_redirect',
                'groups/3631'
            ),
            new Link(
                'APoG',
                LinkType.Mail,
                'contact@apog-grenoble.org',
                'contact@apog-grenoble.org'
            )
        ],
        [], 
        [
            'L’Association des Anciens de Polytech Grenoble (APoG) c’est la structure qui te permet de garder un pied à Polytech même après avoir fini tes études. Elle fédère les diplômés de Polytech Grenoble et suit leur parcours après l’école. D\'ailleurs, quelle que soit leur filière ou leur année de diplôme, ils sont toujours partants pour se retrouver lors d\'un petit afterwork !', 
            'Alors, certes, tu es encore jeune et trois belles années t’attendent (voire plus pour les gourmands) et tu ne vois pas en quoi l’APoG te concerne ? Et bien, crois-le ou non, mais le temps à Polytech passe très très vite, au grand désespoir des anciens…', 
            'Alors si tu veux prévoir ta future vie active et rencontrer des diplômés (de ta région au 3620) pour élargir ton réseau, trouver un stage, avoir des conseils en tout genre, n’hésite pas à suivre l’APoG sur ses différents réseaux sociaux !'
        ]
    ),
    new Association(
        'Poly’raid handi-valide', 
        '', 
        'indep/polyraid.png', 
        [
            new Link(
                'Poly’raid',
                LinkType.Facebook,
                'https://www.facebook.com/polyraid/',
                '1575651439387387'
            ),
            new Link(
                'Poly’raid',
                LinkType.Instagram,
                'https://www.instagram.com/polyraid/',
                'polyraid'
            ),
            new Link(
                'Poly’raid',
                LinkType.WebSite,
                'https://assopolyraid.wixsite.com/polyraid#',
                ''
            )
        ],
        [], 
        [
            'Nous sommes un groupe d’étudiants de Polytech Grenoble. Notre but à travers cette association est d’organiser un raid handi-valide Poly’Raid, accessible à tous les étudiants et personnels de l’Université de Grenoble-Alpes, aux personnels des entreprises partenaires de l’école Polytech Grenoble et aux personnels des sponsors de l’événement.', 
            'Ce raid a un double objectif : • Faire évoluer les mentalités et les représentations sociales sur le handicap • Permettre à chacun de se dépasser en réussissant ensemble des épreuves où l’esprit du sport est visible.', 
            'Evénement le 10 et 11 octobre 2020 : L’association te donne rendez-vous le week-end du 10 et 11 octobre pour un défi collectif et solidaire qui va connecter tout le réseau Polytech ! Rejoins tes Polypotes pour cumuler le plus de kilomètres dans la discipline de ton choix.', 
            'Organisation de l’édition 2021 : Rejoins l’équipe du Poly’Raid si tu es en IESE ou MAT pour organiser le raid de l’édition 2021 lors des projets collectifs.'
        ]
    ),
    new Association(
        'JPP', 
        'Journée Polytech pro', 
        'indep/jpp.png', 
        [
            new Link(
                'JPP',
                LinkType.Facebook,
                'https://www.facebook.com/JourneePolytechPro/',
                '974165652753766'
            )
        ],
        [], 
        [
            'Tu t’inquiètes déjà de ne pas trouver un stage ou un emploi après ton diplôme ?', 
            'Pas de soucis, viens rencontrer différentes entreprises de ta spécialité lors de la Journée Polytech Pro 2020 online, le mardi 17 novembre 2020.', 
            'Lors de la JPP 2020, tu pourras discuter avec les entreprises, déposer des CV pour tes stages ainsi que participer à différentes activités au cours de la journée. ', 
            'Si tu désires plus d’informations ou si tu veux faire partie de l’équipe organisatrice, rejoins Journée Polytech Pro sur Facebook ! '
        ]
    ),
    new Association(
        'LINKO', 
        'Junior entreprise', 
        'indep/linko.png', 
        [
            new Link(
                'LINKO',
                LinkType.Facebook,
                'https://www.facebook.com/Linko-101609464936778/',
                '101609464936778'
            )
        ],
        [], 
        [
            'Tu veux utiliser tes compétences et te faire un peu d’argent ? Linko est la nouvelle association de consulting à Polytech Grenoble. En nous rejoignant tu pourras réaliser des projets rémunérés dans le numérique, proposés par des entreprises partenaires. Tu pourras ajouter quelques lignes sur ton CV, gagner quelques compétences mais aussi participer à des événements plus décontractés.', 
            'Tu veux en savoir plus ? Nous porterons notre veste avec logo dans l’école, ou viens en salle 30 pour nous rencontrer et échanger.'
        ]
    ),
    new Association(
        'Le Blog de polytech', 
        '', 
        'indep/blog_polytech.png', 
        [
            new Link(
                'Le Blog de polytech',
                LinkType.Facebook,
                'https://www.facebook.com/BlogPolytechGrenoble/',
                '1848486285265711'
            ),
            new Link(
                'Le Blog de polytech',
                LinkType.WebSite,
                'https://blog-polytech-grenoble.fr',
                ''
            ),
            new Link(
                'Le Blog de polytech',
                LinkType.Mail,
                'contact@blog-polytech-grenoble.fr',
                'contact@blog-polytech-grenoble.fr'
            )
        ],
        [], 
        [
            'Le Blog de Polytech est un club à part entière de l’école. Ne faisant ni partie du BDA ni du BDS, il a pour mission de couvrir la vie de l’école, d’informer, de réunir et de partager autour d’articles publiés sur le Blog. Mais quel genre d’articles peut-on y trouver exactement ? Des conseils, des retours d’expérience, des articles en anglais, des interviews, des coups de gueule, des recettes, des événements...', 
            'C’est une plateforme qui se veut ouverte à tous·tes et c’est VOUS qui la ferez vivre et prospérer ! Membres du club ou pas, vous pouvez soumettre vos idées ou articles par mail ou par message sur la page. Ne soyez pas timides, toutes sortes d’articles sont publiés – même non liés à Polytech. Pour être averti·e·s des nouveaux articles suivez notre Facebook !', 
            'N’hésite pas à contacter les respos du Blog pour plus d’informations !'
        ]
    )
]