import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AgendaComponent } from './components/agenda/agenda.component';
import { ShortcutComponent } from './components/shortcut/shortcut.component';
import { NewsComponent } from './components/news/news.component';
import { BranchsComponent } from './components/branchs/branchs.component';
import { AssociationsComponent } from './components/associations/associations.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';

import { EventsComponent } from './components/events/events.component';
import { SongsComponent } from './components/songs/songs.component';
import { TheCityComponent } from './components/the-city/the-city.component';
import { PartnershipsComponent } from './components/partnerships/partnerships.component';
import { DiscountCardsComponent } from './components/discount-cards/discount-cards.component';
import { AgendaRoutingModule } from './components/agenda/agenda-routing.module';
import { PageNotFoundComponent } from '@shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'agenda', component: AgendaComponent, 
    loadChildren: () => AgendaRoutingModule
  },
  { path: 'shortcuts', component: ShortcutComponent },
  { path: 'news', component: NewsComponent },
  { path: 'branchs', component: BranchsComponent },
  { path: 'associations', component: AssociationsComponent },
  { path: 'contact', component: ContactComponent },

  { path: 'events', component: EventsComponent },
  { path: 'songs', component: SongsComponent },
  { path: 'the-city', component: TheCityComponent },
  { path: 'partnerships', component: PartnershipsComponent },
  { path: 'discount-cards', component: DiscountCardsComponent },

  { path: '' , pathMatch: 'full', redirectTo : 'home' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})

export class AppRoutingModule { }